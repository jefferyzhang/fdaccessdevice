#include "stdafx.h"
#include <tchar.h>
#include <atlstr.h>
#include <usbioctl.h>
#include "cheadercommon.h"

extern int _label;

namespace Utiltools{
#pragma warning(disable: 4793)
	void logIt(TCHAR* fmt, ...)
	{
		va_list args;

		CString sLog;
		va_start(args, fmt);
		sLog.FormatV(fmt, args);
		va_end(args);
		CString sLogpid;
		sLogpid.Format(_T("[Label_%d]:%s"),_label, sLog);
		OutputDebugString(sLogpid);
	}

	BOOL IsConnectDev(int nPort, TCHAR *szHubName)
	{
		if ((nPort == 0) || (_tcslen(szHubName) == 0))
		{
			logIt(_T("IsConnectDev param missing error."));
			return FALSE;
		}
		BOOL bRet = TRUE;
		TCHAR symblName[1024] = { 0 };

		if ((_tcsncicmp(szHubName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(szHubName, _T("\\\\.\\"), 4) == 0))
		{
			_stprintf_s(symblName, _T("%s"), szHubName);
		}
		else
		{
			_stprintf_s(symblName, _T("%s%s"), _T("\\\\?\\"), szHubName);
		}

		HANDLE hHubDev = CreateFile(symblName,
			GENERIC_WRITE,
			FILE_SHARE_WRITE,
			NULL,
			OPEN_EXISTING,
			0,
			NULL);
		if (hHubDev != INVALID_HANDLE_VALUE)
		{
			DWORD data_len = sizeof(USB_NODE_CONNECTION_INFORMATION_EX) + sizeof(USB_PIPE_INFO) * 32;
			UCHAR ConnectInfoBuf[sizeof(USB_NODE_CONNECTION_INFORMATION_EX) + sizeof(USB_PIPE_INFO) * 32] = { 0 };
			PUSB_NODE_CONNECTION_INFORMATION_EX conn_info = (PUSB_NODE_CONNECTION_INFORMATION_EX)(ConnectInfoBuf);
			conn_info->ConnectionIndex = nPort;

			if (!DeviceIoControl(hHubDev, IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX, conn_info, data_len, conn_info, data_len, &data_len, 0))
			{
				logIt(_T("IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX error %d"), GetLastError());
			}
			else
			{
				logIt(_T("device VID=%04x PID=%04x connect status: %d\n"), conn_info->DeviceDescriptor.idVendor,conn_info->DeviceDescriptor.idProduct, conn_info->ConnectionStatus);
				if (conn_info->ConnectionStatus == NoDeviceConnected)
				{
					bRet = FALSE;
				}
			}
			CloseHandle(hHubDev);
		}
		else
		{
			logIt(_T("device connect CreateFile: %d\n"), GetLastError());
			bRet = FALSE;
		}

		logIt(_T("IsConnectDev -- %s"), bRet ? _T("TRUE") : _T("FALSE"));
		return bRet;
	}

}
