#pragma once 
using namespace System;
using namespace System::Text::RegularExpressions;

void logIt(String^ s);
void SetExePath(String^ exepath);

Boolean CheckApp(String^ sType, String^ sId, String^ sBundle);
int InstallApp(String^ sType, String^ sId, String^ sPcFileName, String^ vpp, String^ loc, String^ appname);
int UnInstallApp(String^ sType, String^ sId, String^ sBundle, String^ vpp, String^ loc, String^ appname);
int PhoneFileExist(String^ sType, String^ sId, String^ sPhoneFileName, String^ sBundle);
int UploadFile(String^ sType, String^ sId, String^ sPcFileName, String^ sDevFileName, String^ sBundle);
int DownloadFile(String^ sType, String^ sId, String^ sPcFileName, String^ sDevFileName, String^ sBundle);
int CheckStatus(String^ sType, int nPort, String^ Hubname, String^ sId, String^ sDevFileName, String^ sBundle, int nIntervalTime = 5000);
int PhoneFileDelete(String^ sType, String^ sId, String^ sPhoneFileName, String^ sBundle);
int SendAssociateRevoke(BOOL bRevoke, String^ sn, String^ appname, String^ loc);