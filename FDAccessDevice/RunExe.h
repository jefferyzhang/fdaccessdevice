#pragma once
using namespace System;
using namespace System::Diagnostics;
using namespace System::Threading;
using namespace System::Text;
using namespace System::Collections::Generic;
/// <summary>
/// Summary for Runexe
/// </summary>
public ref class CRunExe
{
public:
	CRunExe()
	{
		//_quit = false;
		_logError = false;
	}
	int startRunExe(String^ exepath, String^ sparam, int timeout);
	List<String^>^ getExeStdOut();

	void SetErrorLogOnly()
	{
		_logError = true;
	}

protected:
	/// <summary>
	/// Clean up any resources being used.
	/// </summary>
	~CRunExe()
	{

	}

private:
	/// <summary>
	/// Required designer variable.
	/// </summary>
	//static Boolean _quit;
	//static AutoResetEvent^ runningEvent;
	static List<String^>^  sbExeOut;

	static Boolean _logError;
	static DateTime last_output;
	static System::Threading::ManualResetEvent^ ev;
	static int Exitcode = -1;
#pragma region  generated code
	static void DataReceived_Handler(Object^ sender, DataReceivedEventArgs^ e);
	//static void Exited_startiDeviceUtilCore(Object^ sender, EventArgs^ e);
#pragma endregion
};