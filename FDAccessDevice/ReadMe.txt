		"\n"
		"commands:\n"
		"  -check \n                   - check apps \n"
		"		-bundle=x    app id, apple device is Bundle, android is package name\n"
		"  -upload \n                  - upload file \n"
		"		-bundle=x    app id, apple device is Bundle, android is package name\n"
		"		-pcfn=x      pc file name.\n"
		"		-devfn=x     device file name.\n"
		"  -download \n                - download file \n"
		"		-bundle=x    app id, apple device is Bundle, android is package name\n"
		"		-pcfn=x      pc file name.\n"
		"		-devfn=x     device file name.\n"
		"  -status \n                  - wait right status exit \n"
		"		-devfn=x     device file name.\n"
		"  -install \n				   - install apps \n"
		"		-pcfn=x      pc file name.\n"
		"\n"
		"global args:\n"
		"-utilpath=X   X is adb path or ideviceutilcore.exe path. %APSTHOME% or current exe Folder."
		"-label=x     x is number of label.\n"
		"-hubname=x   \n"
		"-hubport=x   \n"
		"-type=x      x is [Android|Apple]\n"
		"-serail=x	  x is UDID if type is apple, x is serial if type is android.\n"

Android device command line:
	-check -serail=5de0f937 -bundle=com.futuredial.fdbox721 -type=Android
	-upload -serail=5de0f937 -pcfn=J:\temp\adbserverport.ini -devfn=/data/local/tmp/adbser.ini -type=Android
	-download -serail=5de0f937 -pcfn=J:\temp\aaaaa.ini -devfn=/data/local/tmp/adbser.ini -type=Android
	-install -serail=5de0f937 -pcfn=J:\temp\aaaaa.apk -type=Android
	-status -serail=5de0f937 -devfn=/data/local/tmp/adbser.ini -type=Android -hubport=3 -hubname="USB#ROOT_HUB20#4&291be188&0#{f18a0e88-c30c-11d0-8815-00a0c906bed8}"

Apple Device:
	-install -serail=6ae3db97903bc3f74841ecb4c0db5e415d05c4b5 -pcfn=J:\temp\FunctionTest-Distribution-2.3.2.ipa -utilpath=J:\Works\iDeviceUtilCore\Release\iDeviceUtilCore.exe -type=Apple
	-check -serail=6ae3db97903bc3f74841ecb4c0db5e415d05c4b5 -bundle=sujian.Function-Test -type=Apple -utilpath=J:\Works\iDeviceUtilCore\Release\iDeviceUtilCore.exe
	-upload -serail=6ae3db97903bc3f74841ecb4c0db5e415d05c4b5 -pcfn=J:\temp\adbserverport.ini -devfn=/Documents -bundle=sujian.Function-Test -type=Apple -utilpath=J:\Works\iDeviceUtilCore\Release\iDeviceUtilCore.exe
	-download -serail=6ae3db97903bc3f74841ecb4c0db5e415d05c4b5 -pcfn=J:\temp\aaaaa.ini -devfn=/Documents/1.plist -bundle=sujian.Function-Test -type=Apple -utilpath=J:\Works\iDeviceUtilCore\Release\iDeviceUtilCore.exe
	-status -serail=6ae3db97903bc3f74841ecb4c0db5e415d05c4b5 -devfn=/Documents/1.plist -bundle=sujian.Function-Test -type=Apple -hubport=3 -hubname="USB#ROOT_HUB20#4&3215e6&0#{f18a0e88-c30c-11d0-8815-00a0c906bed8}" -utilpath=J:\Works\iDeviceUtilCore\Release\iDeviceUtilCore.exe