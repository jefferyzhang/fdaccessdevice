#pragma once 
#include <Windows.h>
#include <tchar.h>
#include <Shlwapi.h>
#include <atlstr.h>

#pragma comment(lib, "Shlwapi.lib")

namespace Utiltools{
	void logIt(TCHAR* fmt, ...);
	BOOL IsConnectDev(int nPort, TCHAR *szHubName);
};

void GetExePath();