#include "stdafx.h"
#include "cheadercommon.h"
#include "clicommon.h"
#include "RunExe.h"
#include <msclr\marshal.h>
#include <msclr\marshal_cppstd.h>
#include <vcclr.h>

using namespace System::Net;
using namespace System::IO;
using namespace System::Web::Script::Serialization;
using namespace System::Net::Security;
using namespace System::Security::Cryptography::X509Certificates;
using namespace Runtime::InteropServices;
using namespace System::Security::Cryptography;

static bool ValidateServerCertificate(
	Object^ sender,
	X509Certificate^ certificate,
	X509Chain^ chain,
	SslPolicyErrors sslPolicyErrors)
{
	logIt(certificate->ToString());
	return true;
}

Dictionary<String^, Object^>^ getInfo(String^ appname, String^ loc) {
	Dictionary<String^, Object^>^ infos = gcnew Dictionary<String^, Object^>();
	String^ configfile = Path::Combine(Environment::ExpandEnvironmentVariables("%APSTHOME%"), "config.ini");
	if (File::Exists(configfile)) {
/*
"c": 41,       companyid                                           # Company id
"s": 1,        siteid                                            # Site id
"g": 2,                                                    # Work station group id (optional)
"p": 2,         productid                                           # CMC product id
*/
		const TCHAR* lpFileName =
			(const TCHAR*)(Marshal::StringToHGlobalUni(configfile).ToPointer());
		infos["c"] = GetPrivateProfileInt(_T("config"), _T("companyid"), 0, lpFileName);
		infos["s"] = GetPrivateProfileInt(_T("config"), _T("siteid"), 0, lpFileName);
		//infos["g"] = GetPrivateProfileInt(_T("config"), _T(""), 0, lpFileName);
		infos["p"] = GetPrivateProfileInt(_T("config"), _T("productid"), 0, lpFileName);
	}
	infos["app"] = appname;
	infos["loc"] = loc;
	return infos;
}

String^ Encrypt(String^ toEncrypt)
{
	array<byte>^ keyArray;
	array<byte>^ toEncryptArray = UTF8Encoding::UTF8->GetBytes(toEncrypt);


	String^ key = "fdzt-506";
	//System.Windows.Forms.MessageBox.Show(key);
	//If hashing use get hashcode regards to your key

	keyArray = UTF8Encoding::UTF8->GetBytes(key);

	DESCryptoServiceProvider^ tdes = gcnew DESCryptoServiceProvider();
	//set the secret key for the tripleDES algorithm
	tdes->Key = keyArray;
	//mode of operation. there are other 4 modes. We choose ECB(Electronic code Book)
	tdes->Mode = CipherMode::ECB;
	//padding mode(if any extra byte added)
	tdes->Padding = PaddingMode::PKCS7;

	ICryptoTransform^ cTransform = tdes->CreateEncryptor();
	//transform the specified region of bytes array to resultArray
	array<byte>^ resultArray = cTransform->TransformFinalBlock(toEncryptArray, 0, toEncryptArray->Length);
	//Release resources held by TripleDes Encryptor
	tdes->Clear();
	//Return the encrypted data into unreadable string format
	return Convert::ToBase64String(resultArray, 0, resultArray->Length);
}

String^ GenerateAuthorization(String^ appname, String^ loc) {
	Dictionary<String^, Object^>^ data = getInfo(appname, loc);
	auto serializer = gcnew JavaScriptSerializer();
	String^ org_data = serializer->Serialize(data);
	return Encrypt(org_data);
}

int SendAssociateRevoke(BOOL bRevoke, String^ sn, String^ appname, String^ loc) {

	logIt(String::Format("revoke={0}, sn={1}, vppname={2}, loc={3}", bRevoke, sn, appname, loc));
	// this is where we will send it
	String^ uri = "https://cfzt.futuredial.com/api/assets/associate/";
	if (bRevoke) {
		uri = "https://cfzt.futuredial.com/api/assets/revoke/";
	}
	try {
		ServicePointManager::SecurityProtocol = SecurityProtocolType::Tls12;
		ServicePointManager::ServerCertificateValidationCallback = gcnew RemoteCertificateValidationCallback(ValidateServerCertificate);

		// create a request
		HttpWebRequest^ request = (HttpWebRequest^)WebRequest::Create(uri);
		request->KeepAlive = false;
		//request->ProtocolVersion = HttpVersion::Version10 | HttpVersion::Version11;
		request->Method = "POST";

		Dictionary<String^, Object^>^ data = gcnew Dictionary<String^, Object^>();
		List<String^>^ list = gcnew List<String^>();
		list->Add(sn);
		data["serialnumbers"] = list;
		auto serializer = gcnew JavaScriptSerializer();
		String^ post_data = serializer->Serialize(data);
		logIt("Post data: "+post_data);

		// turn our request string into a byte stream
		array<byte>^ postBytes = Encoding::ASCII->GetBytes(post_data);

		request->ContentType = "application/json";
		// this is important - make sure you specify type this way
		String^ bearer = GenerateAuthorization(appname, loc);
		request->Headers->Add("Authorization", String::Format("Bearer {0}", bearer));
		request->ContentLength = postBytes->Length;
		Stream^ requestStream = request->GetRequestStream();

		// now send it
		requestStream->Write(postBytes, 0, postBytes->Length);
		requestStream->Close();

		// grab te response and print it out to the console along with the status code
		HttpWebResponse^ response = (HttpWebResponse^)(request->GetResponse());
		logIt((gcnew StreamReader(response->GetResponseStream()))->ReadToEnd());
		if (response->StatusCode == HttpStatusCode::OK) {
			return 0;
		}
	}
	catch (Exception^ e) {
		logIt(e->ToString());
	}
	return 1;
}
