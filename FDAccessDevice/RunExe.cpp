#include "stdafx.h"
#include "RunExe.h"
#include "clicommon.h"

using namespace System::IO;

//void CRunExe::Exited_startiDeviceUtilCore(Object^ sender, EventArgs^ e)
//{
//	logIt("Exited_startRunExe");
//	_quit = true;
//	runningEvent->Set();
//}

void CRunExe::DataReceived_Handler(Object^ sender, DataReceivedEventArgs^ e)
{
//	runningEvent->Set();
	//if (!String::IsNullOrEmpty(e->Data))
	//{
	//	String^ ss = e->Data;
	//	sbExeOut->Add(ss);
	//	logIt(ss);
	//}

	last_output = DateTime::Now;
	if (!String::IsNullOrEmpty(e->Data))
	{
		logIt(String::Format("[runExe]: {0}", e->Data));
		sbExeOut->Add(e->Data);
	}
	if (e->Data == nullptr)
		ev->Set();
}

List<String^>^ CRunExe::getExeStdOut()
{
	return sbExeOut;
}

int CRunExe::startRunExe(String^ exepath, String^ sparam, int timeout)
{
	Exitcode = 1;
//	_quit = false;
	logIt("startRunExe: ++ " + exepath);
	logIt("Run Start Time :" + DateTime::Now.ToString());
	//runningEvent = gcnew AutoResetEvent(false);
	sbExeOut = gcnew List<String^>();
	if (System::IO::File::Exists(exepath))
	{
		last_output = DateTime::Now;
		DateTime _start = DateTime::Now;
		ev = gcnew System::Threading::ManualResetEvent(false);
		logIt(String::Format("launch exe with {0}", sparam));
		FileVersionInfo^ vinfo = FileVersionInfo::GetVersionInfo(exepath);
		logIt(String::Format("{0} version {1}", System::IO::Path::GetFileName(vinfo->FileName), vinfo->FileVersion));
		Process^ p = gcnew Process();
		p->StartInfo->FileName = exepath;// "cmd.exe";
		p->StartInfo->Arguments = sparam;// String::Format("/C \"{0}\" {1}", exepath, sparam);
		p->StartInfo->UseShellExecute = false;
		p->StartInfo->CreateNoWindow = true;
		p->StartInfo->WindowStyle = System::Diagnostics::ProcessWindowStyle::Hidden;
		p->StartInfo->RedirectStandardError = true;
		if (!_logError)
		{
			p->StartInfo->RedirectStandardOutput = true;
		}
		//p->Exited += gcnew EventHandler(Exited_startiDeviceUtilCore);
		////p.EnableRaisingEvents = true;

		if (!_logError)
		{
			p->OutputDataReceived += gcnew DataReceivedEventHandler(DataReceived_Handler);
		}
		p->ErrorDataReceived += gcnew DataReceivedEventHandler(DataReceived_Handler);
		
		p->Start();
		p->BeginOutputReadLine();
		p->BeginErrorReadLine();
		

		logIt(String::Format("{0}: pid={1} ", Path::GetFileName(vinfo->FileName), p->Id));

		bool process_terminated = false;
		bool proces_stdout_cloded = false;
		bool proces_has_killed = false;
		while (!proces_stdout_cloded || !process_terminated)
		{
			if (p->HasExited)
			{
				// process is terminated
				process_terminated = true;
				logIt(String::Format("[runExe]: process is going to terminate."));
			}
			if (ev->WaitOne(1000))
			{
				// stdout is colsed
				proces_stdout_cloded = true;
				Sleep(1000);
				if (!p->HasExited)
				{
					p->Kill();
				}
				break;
			}
			if ((DateTime::Now - last_output).TotalMilliseconds > timeout)
			{
				logIt(String::Format("[runExe]: there are {0} milliseconds no response. timeout?", timeout));
				// no output received within timeout milliseconds
				if (!p->HasExited)
				{
					Exitcode = 1460;
					p->Kill();
					proces_has_killed = true;
					logIt(String::Format("[runExe]: process is going to be killed due to timeout."));
				}
				break;
			}
		}
		if (!proces_has_killed)
			Exitcode = p->ExitCode;
	}
	else
	{
		logIt("not Found exe file.");
		Exitcode = 20011;
	}
	logIt("Run End Time :" + DateTime::Now.ToString());
	logIt(String::Format("param: {0} return {1}", sparam, Exitcode));
	return Exitcode;
}