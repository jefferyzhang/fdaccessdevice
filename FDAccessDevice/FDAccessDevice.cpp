// FDAccessDevice.cpp : main project file.

#include "stdafx.h"
#include <stdio.h>
#include "cheadercommon.h"

#include "clicommon.h"

using namespace System;

int _label = 0;


void logIt(String^ s)
{
	System::Diagnostics::Trace::WriteLine(String::Format("[Label_{0}]: {1}", _label, s));
}


String^ get_AppleInstallDir(String^ moduleName)
{
	String^ ret = String::Empty;
	try
	{
		String^ Keypath = "SOFTWARE\\Apple Inc.\\{0}";
		Microsoft::Win32::RegistryKey^ regKey = Microsoft::Win32::Registry::LocalMachine->OpenSubKey(String::Format(Keypath, moduleName));
		ret = (String^)regKey->GetValue("InstallDir");
		regKey->Close();
	}
	catch (...) {}
	return ret;
}

void Usage()
{
	fprintf(stderr, 
		"Usage: <command> [<arg>...]");

	fprintf(stderr,
		"\n"
		"commands:\n"
		"  -check \n                   - check apps \n"
		"		-bundle=x    app id, apple device is Bundle, android is package name\n"
		"  -upload \n                  - upload file \n"
		"		-bundle=x    app id, apple device is Bundle, android is package name\n"
		"		-pcfn=x      pc file name.\n"
		"		-devfn=x     device file name.\n"
		"  -download \n                - download file \n"
		"		-bundle=x    app id, apple device is Bundle, android is package name\n"
		"		-pcfn=x      pc file name.\n"
		"		-devfn=x     device file name.\n"
		"  -status \n                  - wait right status exit \n"
		"		-devfn=x     device file name.\n"
		"  -install \n				   - install apps \n"
		"		-pcfn=x      pc file name.\n"
		"  -uninstall \n				   - uninstall apps \n"
		"		-bundle=x    app id, apple device is Bundle, android is package name\n"
		"  -delete \n				   - delete files \n"
		"		-devfn=x     device file name.\n"
		"  -fileexist \n				   - fileexist files \n"
		"		-devfn=x     device file name.\n"
		"\n"
		"global args:\n"
		"-utilpath=X   X is adb path or ideviceutilcore.exe path."
		"-label=x     x is number of label.\n"
		"-hubname=x   \n"
		"-hubport=x   \n"
		"-type=x      x is [Android|Apple]\n"
		"-timeout=x      x is seconds\n"
		"-serail=x	  x is UDID if type is apple, x is serial if type is android.\n"
		"-echo=x	  1 or 0, default 0. if 1, use echo replace pull or push.\n"
		);
}

int main(array<System::String ^> ^args)
{
	logIt(String::Format("{0} start: ++ version: {1}",
		System::IO::Path::GetFileName(System::Diagnostics::Process::GetCurrentProcess()->MainModule->FileName),
		System::Diagnostics::Process::GetCurrentProcess()->MainModule->FileVersionInfo->FileVersion));


	String^ sAMDSDir = get_AppleInstallDir("Apple Mobile Device Support");
	Environment::SetEnvironmentVariable("Path", String::Join(";", gcnew array<String^> { Environment::GetEnvironmentVariable("Path"), get_AppleInstallDir("Apple Application Support"), sAMDSDir }));
	System::Configuration::Install::InstallContext^ _param = gcnew System::Configuration::Install::InstallContext(nullptr, args);

	int nHubport = 0;
	String^ sHubName = "";
	String^ sType = "";
	String^ sSerial = "";
	try{
		GetExePath();
	}
	catch (...){}
	sType = "Android";
	if (_param->IsParameterTrue("debug"))
	{
		Console::WriteLine("wait for debug. press any key to contiune.");
		Console::ReadKey();
	}

	if (_param->IsParameterTrue("test")) {
		String^ vpp = ""; //iPhone Only, it is iPhone SerialNumber
		if (_param->Parameters->ContainsKey("vpp"))
		{
			vpp = _param->Parameters["vpp"];
		}
		String^ location = "";
		if (_param->Parameters->ContainsKey("loc"))
		{
			location = _param->Parameters["loc"];
		}
		String^ appname = "";
		if (_param->Parameters->ContainsKey("vppname"))
		{
			appname = _param->Parameters["vppname"];
		}
		SendAssociateRevoke(false, vpp, appname, location);
		return 0;
	}

	if (_param->Parameters->ContainsKey("echo"))
	{
		extern int nEchoType;
		nEchoType = Convert::ToInt32(_param->Parameters["echo"]);
	}
	if (_param->Parameters->ContainsKey("utilpath"))
	{
		SetExePath(_param->Parameters["utilpath"]);
	}
	if (_param->Parameters->ContainsKey("label"))
	{
		_label = Convert::ToInt32(_param->Parameters["label"]);
	}
	if (_param->Parameters->ContainsKey("hubport"))
	{
		nHubport = Convert::ToInt32(_param->Parameters["hubport"]);
	}
	if (_param->Parameters->ContainsKey("hubname"))
	{
		sHubName = _param->Parameters["hubname"];
	}
	if (_param->Parameters->ContainsKey("type"))
	{
		sType = _param->Parameters["type"];
	}
	if (_param->Parameters->ContainsKey("serail"))
	{
		sSerial = _param->Parameters["serail"];
	}

	if (String::IsNullOrEmpty(sType) || String::IsNullOrEmpty(sSerial))
	{
		Usage();
		return 1;
	}

	String^ sBundle = "";
	String^ sPCFileName = "";
	String^ sDeviceFileName = "";

	if (_param->Parameters->ContainsKey("pcfn"))
	{
		sPCFileName = _param->Parameters["pcfn"];
		sPCFileName = Environment::ExpandEnvironmentVariables(sPCFileName);
	}
	if (_param->Parameters->ContainsKey("devfn"))
	{
		sDeviceFileName = _param->Parameters["devfn"];
	}

	if (_param->IsParameterTrue("check"))
	{
		if (_param->Parameters->ContainsKey("bundle"))
		{
			sBundle = _param->Parameters["bundle"];
		}
		return CheckApp(sType, sSerial, sBundle)?0:1;
	}
	else if (_param->IsParameterTrue("upload"))
	{
		if (_param->Parameters->ContainsKey("bundle"))
		{
			sBundle = _param->Parameters["bundle"];
		}

		if (String::IsNullOrEmpty(sPCFileName) || String::IsNullOrEmpty(sDeviceFileName))
		{
			Usage();
			return 1;
		}
		return UploadFile(sType, sSerial, sPCFileName, sDeviceFileName, sBundle);
	}
	else if (_param->IsParameterTrue("download"))
	{
		if (_param->Parameters->ContainsKey("bundle"))
		{
			sBundle = _param->Parameters["bundle"];
		}

		if (String::IsNullOrEmpty(sPCFileName) || String::IsNullOrEmpty(sDeviceFileName))
		{
			Usage();
			return 1;
		}
		return DownloadFile(sType, sSerial, sPCFileName, sDeviceFileName, sBundle);
	}
	else if (_param->IsParameterTrue("status"))
	{
		if (String::IsNullOrEmpty(sDeviceFileName) || String::IsNullOrEmpty(sHubName) || nHubport == 0)
		{
			Usage();
			return 1;
		}
		if (_param->Parameters->ContainsKey("bundle"))
		{
			sBundle = _param->Parameters["bundle"];
		}
		int nTimeout = 20000;
		if (_param->Parameters->ContainsKey("timeout"))
		{
			nTimeout = Convert::ToInt32(_param->Parameters["timeout"]);
			nTimeout = nTimeout < 2 ? 2000 : nTimeout * 1000;
			extern int g_nTimeout;
			g_nTimeout = nTimeout;
		}
		return CheckStatus(sType, nHubport, sHubName, sSerial, sDeviceFileName, sBundle, nTimeout);
	}
	else if (_param->IsParameterTrue("install"))
	{
		if (String::IsNullOrEmpty(sPCFileName))
		{
			Usage();
			return 1;
		}
		String^ vpp = ""; //iPhone Only, it is iPhone SerialNumber
		if (_param->Parameters->ContainsKey("vpp"))
		{
			vpp = _param->Parameters["vpp"];
		}
		String^ location = ""; 
		if (_param->Parameters->ContainsKey("loc"))
		{
			location = _param->Parameters["loc"];
		}
		String^ appname = "";
		if (_param->Parameters->ContainsKey("vppname"))
		{
			appname = _param->Parameters["vppname"];
		}
		return InstallApp(sType, sSerial, sPCFileName, vpp, location, appname);
	}
	else if (_param->IsParameterTrue("uninstall"))
	{
		if (_param->Parameters->ContainsKey("bundle"))
		{
			sBundle = _param->Parameters["bundle"];
		}
		String^ vpp = "";
		if (_param->Parameters->ContainsKey("vpp"))
		{
			vpp = _param->Parameters["vpp"];
		}
		String^ location = "";
		if (_param->Parameters->ContainsKey("loc"))
		{
			location = _param->Parameters["loc"];
		}
		String^ appname = "";
		if (_param->Parameters->ContainsKey("vppname"))
		{
			location = _param->Parameters["vppname"];
		}
		return UnInstallApp(sType, sSerial, sBundle, vpp, location, appname);
	}
	else if (_param->IsParameterTrue("fileexist"))
	{
		if (_param->Parameters->ContainsKey("bundle"))
		{
			sBundle = _param->Parameters["bundle"];
		}
		if (String::IsNullOrEmpty(sDeviceFileName))
		{
			Usage();
			return 1;
		}
		return PhoneFileExist(sType, sSerial, sDeviceFileName, sBundle);
	}
	else if (_param->IsParameterTrue("delete"))
	{
		if (_param->Parameters->ContainsKey("bundle"))
		{
			sBundle = _param->Parameters["bundle"];
		}
		if (String::IsNullOrEmpty(sDeviceFileName))
		{
			Usage();
			return 1;
		}
		return PhoneFileDelete(sType, sSerial, sDeviceFileName, sBundle);
	}
	return 0;
}
