#include "stdafx.h"
#include "cheadercommon.h"
#include "clicommon.h"
#include "RunExe.h"
#include <msclr\marshal.h>
#include <msclr\marshal_cppstd.h>
#include <vcclr.h>

#include "clicommon.h"



#define ADBEXE				L"adb.exe"
#define IDEVICEUTILCORE		L"iDeviceUtilCore.exe"

TCHAR adbpath[MAX_PATH] = { 0 };
TCHAR idevceutilcorepath[MAX_PATH] = { 0 };

int nEchoType = 0;

int g_nTimeout = 5 * 60 * 1000;

void GetExePath()
{
	String^ sApsthome = Environment::GetEnvironmentVariable("APSTHOME");
	if (_tcslen(adbpath) == 0 || !PathFileExists(adbpath))
	{
		//pin_ptr<WCHAR> content = PtrToStringChars(curItem);
		//std::wstring result(content, curItem->Length);
		if (System::IO::File::Exists(System::IO::Path::Combine(sApsthome, ADBEXE)))
		{
			pin_ptr<const TCHAR> content = PtrToStringChars(System::IO::Path::Combine(sApsthome, ADBEXE));
			_sntprintf_s(adbpath, MAX_PATH, _T("%s"), content);
		}
		else if (System::IO::File::Exists(System::IO::Path::Combine(System::Reflection::Assembly::GetEntryAssembly()->Location, ADBEXE)))
		{
			//System.Reflection.Assembly.GetEntryAssembly().Location;
			pin_ptr<const TCHAR> content = PtrToStringChars(System::IO::Path::Combine(System::Reflection::Assembly::GetEntryAssembly()->Location, ADBEXE));
			_sntprintf_s(adbpath, MAX_PATH, _T("%s"), content);
		}
		else
		{
			logIt("Can not find adb.exe! please check");
		}
	}

	if (_tcslen(idevceutilcorepath) == 0 || !PathFileExists(idevceutilcorepath))
	{
		//pin_ptr<WCHAR> content = PtrToStringChars(curItem);
		//std::wstring result(content, curItem->Length);
		if (System::IO::File::Exists(System::IO::Path::Combine(System::Reflection::Assembly::GetEntryAssembly()->Location, IDEVICEUTILCORE)))
		{
			//System.Reflection.Assembly.GetEntryAssembly().Location;
			pin_ptr<const TCHAR> content = PtrToStringChars(System::IO::Path::Combine(System::Reflection::Assembly::GetEntryAssembly()->Location, IDEVICEUTILCORE));
			_sntprintf_s(idevceutilcorepath, MAX_PATH, _T("%s"), content);
		}
		else if (System::IO::File::Exists(System::IO::Path::Combine(sApsthome, IDEVICEUTILCORE)))
		{
			pin_ptr<const TCHAR> content = PtrToStringChars(System::IO::Path::Combine(sApsthome, IDEVICEUTILCORE));
			_sntprintf_s(idevceutilcorepath, MAX_PATH, _T("%s"), content);
		}
		else
		{
			logIt("Can not find iDeviceUtilCore.exe! please check");
		}
	}

}

void SetExePath(String^ exepath)
{
	if (System::IO::File::Exists(exepath))
	{
		if (String::Compare(System::IO::Path::GetFileName(exepath),ADBEXE, TRUE) == 0)		
		{
			pin_ptr<const TCHAR> content = PtrToStringChars(exepath);
			_sntprintf_s(adbpath, MAX_PATH, _T("%s"), content);
		}
		else if (String::Compare(System::IO::Path::GetFileName(exepath), IDEVICEUTILCORE, TRUE) == 0)
		{
			pin_ptr<const TCHAR> content = PtrToStringChars(exepath);
			_sntprintf_s(idevceutilcorepath, MAX_PATH, _T("%s"), content);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
//check app
//
Boolean CheckiDeviceApp(String^ sId, String^ sBundle)
{
	//ideviceutilcore.exe -u udid --listapp | findstr ""
	Boolean bRet = FALSE;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" -u {0} --listapp", sId);
	
	if (runExe->startRunExe(gcnew String(idevceutilcorepath), sparm, g_nTimeout) == 0)
	{
		for each (String^ s in runExe->getExeStdOut())
		{
			logIt(s);
			if (s->IndexOf(sBundle)==0)
			{
				bRet = TRUE;
				break;
			}
		}
	}
	else
	{
		for each (String^ s in runExe->getExeStdOut())
		{
			logIt(s);
		}
	}

	return bRet;

}

Boolean CheckAndroidApp(String^ sId, String^ sBundle)
{
	//adb -s shell pm list packages | findstr ""

	Boolean bRet = FALSE;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" shell pm list packages");
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId)+sparm;
	}
	if (runExe->startRunExe(gcnew String(adbpath), sparm, g_nTimeout) == 0)
	{
		for each (String^ s in runExe->getExeStdOut()->ToArray())
		{
			logIt("find data:"+s);
			array<Char>^ chars = { ':'};
			array<String^>^ sps = s->Split(chars, StringSplitOptions::RemoveEmptyEntries);
			if (sps->Length == 2)
			{
				if (String::Compare(sps[1], sBundle, TRUE) == 0)
				{
					bRet = TRUE;
					break;
				}
			}
		}
	}
	else
	{
		for each (String^ s in runExe->getExeStdOut())
		{
			logIt(s);
		}
	}

	return bRet;
}

Boolean CheckApp(String^ sType, String^ sId, String^ sBundle)
{
	Boolean bRet = FALSE;
	logIt(String::Format("CheckApp++ sType={0},sID={1},sBundle={2}",sType, sId, sBundle));
	if (String::Compare(sType, "Apple", TRUE) == 0)
	{
		bRet = CheckiDeviceApp(sId, sBundle);
	}
	else if (String::Compare(sType, "Android", TRUE) == 0)
	{
		bRet = CheckAndroidApp(sId, sBundle);
	}
	return bRet;
}
///////////////////////////////////////////////////////////////////////////////
///install app
//
int SendSucc = 1;
void MyThreadFunc(System::Object^ parameters)
{
	// Unpack the parameters
	array<System::Object^>^ paramsArray = (array<System::Object^>^)parameters;
	bool param1 = (bool)paramsArray[0];
	String^ serialNumber = (String^)paramsArray[1];
	String^ vppname = (String^)paramsArray[2];
	String^ loc = (String^)paramsArray[3];
	if (String::IsNullOrEmpty(serialNumber) || String::IsNullOrEmpty(vppname) || String::IsNullOrEmpty(loc))
	{
		SendSucc = 0;
	}
	else {
		SendSucc = SendAssociateRevoke(param1, serialNumber, vppname, loc);
	}
}

int InstallAppleApp(String^ sId, String^ sPcFileName, String^ serialNumber, String^ loc, String^ vppname)
{
	int nRet = ERROR_INVALID_PARAMETER;
	array<System::Object^>^ paramsArray = gcnew array<System::Object^>{false,serialNumber, vppname, loc};
	Thread^ myThread = gcnew Thread(gcnew ParameterizedThreadStart(MyThreadFunc));
	// Start the thread
	myThread->Start(paramsArray);

	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format("-u {0} --install \"{1}\"", sId, sPcFileName);

	nRet = runExe->startRunExe(gcnew String(idevceutilcorepath), sparm, g_nTimeout);
	if (nRet == 0)
	{
	}
	myThread->Join();
	if (SendSucc != 0) {
		logIt("Thread web request Failed");
		nRet = 20;
	}
	
	return nRet;
}

int InstallAndroidApp(String^ sId, String^ sPcFileName)
{
	int nRet = ERROR_INVALID_PARAMETER;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" install -r -g \"{0}\"", sPcFileName);
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	if ((nRet = runExe->startRunExe(gcnew String(adbpath), sparm, g_nTimeout)) == 0)
	{
		nRet = ERROR_NOT_FOUND;
		for each (String^ s in runExe->getExeStdOut()->ToArray())
		{
			logIt(s);
			if (String::Compare(s, "Success", TRUE) == 0)
			{
				nRet = 0;
				break;
			}
		}
	}
	else
	{
		for each (String^ s in runExe->getExeStdOut()->ToArray())
		{
			logIt(s);
		}
	}
	return nRet;
}

int InstallApp(String^ sType, String^ sId, String^ sPcFileName, String^ serialNumber, String^ loc, String^ appname)
{
	logIt(String::Format("InstallApp++ sType={0},sID={1},pcfile={2}", sType, sId, sPcFileName));
	int nRet = ERROR_NOT_FOUND;
	if (String::Compare(sType, "Apple", TRUE) == 0)
	{
		nRet = InstallAppleApp(sId, sPcFileName, serialNumber, loc, appname);
	}
	else if (String::Compare(sType, "Android", TRUE) == 0)
	{
		nRet = InstallAndroidApp(sId, sPcFileName);
	}
	return nRet;
}
/////////////////////////////////////////////////////////////////////////////////////////////
// uninstall app
//
int  UnInstallAppleApp(String^ sId, String^ sBundle, String^ serialNumber, String^ loc, String^ appname)
{
	int nRet = ERROR_INVALID_PARAMETER;

	array<System::Object^>^ paramsArray = gcnew array<System::Object^>{true,serialNumber, appname, loc};
	Thread^ myThread = gcnew Thread(gcnew ParameterizedThreadStart(MyThreadFunc));
	// Start the thread
	myThread->Start(paramsArray);


	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format("-u {0} --uninstall \"{1}\"", sId, sBundle);

	nRet = runExe->startRunExe(gcnew String(idevceutilcorepath), sparm, g_nTimeout);

	myThread->Join();
	if (SendSucc != 0) {
		logIt("Thread web request Failed");
		nRet = 20;
	}

	return nRet;
}

int UnInstallAndroidApp(String^ sId, String^ sBundle)
{
	int nRet = ERROR_INVALID_PARAMETER;
	CRunExe^ runExe = gcnew CRunExe();
	String^ delimStr = ";:";
	array<Char>^ delimiter = delimStr->ToCharArray();
	array<String^>^ appids = sBundle->Split(delimiter, StringSplitOptions::RemoveEmptyEntries);
	for each(String^ s in appids)
	{
		String^ sparm = String::Format(" uninstall \"{1}\"", sId, sBundle);
		if (!String::IsNullOrEmpty(sId))
		{
			sparm = String::Format("-s {0} ", sId) + sparm;
		}
		nRet = runExe->startRunExe(gcnew String(adbpath), sparm, g_nTimeout);
		if (nRet == ERROR_SUCCESS)
		{
			nRet = ERROR_NOT_FOUND;
			for each (String^ s in runExe->getExeStdOut()->ToArray())
			{
				logIt(s);
				if (String::Compare(s, "Success", TRUE) == 0)
				{
					nRet = ERROR_SUCCESS;
					break;
				}
			}
		}

		if (nRet != ERROR_SUCCESS) break;
	}


	return nRet;
}

int UnInstallApp(String^ sType, String^ sId, String^ sBundle, String^ serialNumber, String^ loc, String^ appname)
{
	logIt(String::Format("UnInstallApp++ sType={0},sID={1},Bundle={2}", sType, sId, sBundle));
	int nRet = ERROR_NOT_FOUND;
	if (String::Compare(sType, "Apple", TRUE) == 0)
	{
		nRet = UnInstallAppleApp(sId, sBundle, serialNumber, loc, appname);
	}
	else if (String::Compare(sType, "Android", TRUE) == 0)
	{
		nRet = UnInstallAndroidApp(sId, sBundle);
	}
	return nRet;
}

/////////////////////////////////////////////////////////////////////////////////////////////
//Device File Exist
//
int PhoneAppleFileExist(String^ sId, String^ sPhoneFileName, String^ sBundle)
{
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" -u {1} --fileexist {0} ", sPhoneFileName, sId);
	if (!String::IsNullOrEmpty(sBundle))
		sparm += String::Format(" --domain {0}", sBundle);
	int err = ERROR_SUCCESS;
	if ((err = runExe->startRunExe(gcnew String(idevceutilcorepath), sparm, g_nTimeout)) == ERROR_SUCCESS)
	{
	}
	if (err == 0xE800005B){
		logIt(String::Format("PhoneAppleFileExist need restart phone"));
	}
	
	return err;
}

int PhoneAndroidFileExist(String^ sId, String^ sPhoneFileName)
{
	int bRet = 0;
	CRunExe^ runExe = gcnew CRunExe();
	//runExe->SetErrorLogOnly();
	String^ sparm = String::Format(" shell cat {0}", sPhoneFileName);
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	if (runExe->startRunExe(gcnew String(adbpath), sparm, g_nTimeout) == ERROR_SUCCESS)
	{
		bRet = 0;
		for each (String^ s in runExe->getExeStdOut()->ToArray())
		{
			logIt(s);
			//if (String::Compare(s, sPhoneFileName, TRUE) == 0)
			if (s->IndexOf("No such file or directory")>=0)
			{
				bRet = 1;
				logIt("find error information. file not exist.");
				break;
			}
			if (s->IndexOf("Permission denied")>=0)
			{
				bRet = ERROR_NOT_SUPPORTED;
				logIt("Cat command not supported");
				break;
			}
		}
	}
	else
	{
		bRet = 1;
		for each (String^ s in runExe->getExeStdOut())
		{
			logIt(s);
			if (s->IndexOf("No such file or directory") >= 0)
			{
				bRet = 1;
				logIt("find error information. file not exist.");
				break;
			}
			if (s->IndexOf("Permission denied") >= 0)
			{
				bRet = ERROR_NOT_SUPPORTED;
				logIt("Cat command not supported");
				break;
			}
		}
	}

	return bRet;
}

int PhoneFileExist(String^ sType, String^ sId, String^ sPhoneFileName, String^ sBundle)
{
	int bRet = 0;
	logIt(String::Format("PhoneFileExist++ sType={0},sID={1},phonefile={2}", sType, sId, sPhoneFileName));
	if (String::Compare(sType, "Apple", TRUE) == 0)
	{
		bRet = PhoneAppleFileExist(sId, sPhoneFileName, sBundle);
	}
	else if (String::Compare(sType, "Android", TRUE) == 0)
	{
		bRet = PhoneAndroidFileExist(sId, sPhoneFileName);
	}
	return bRet;
}

////////////////////////////////////////////////////////////////////////////////////////
///upload file
//
int UploadAppleFile(String^ sId, String^ sPcFileName, String^ sDevFileName, String^ sBundle)
{
	int nRet = ERROR_INVALID_PARAMETER;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm;
	if (String::IsNullOrEmpty(sBundle))
	{
		sparm = String::Format("-u {0} --upload --local \"{1}\" --remote \"{2}\"", sId, sPcFileName, sDevFileName);
	}
	else
	{
		sparm = String::Format("-u {0} --upload2app \"{1}\" --local \"{2}\" --remote \"{3}\"", sId, sBundle, sPcFileName, sDevFileName);
	}
	nRet = runExe->startRunExe(gcnew String(idevceutilcorepath), sparm, g_nTimeout);

	return nRet;
}

int UploadAndroidFile(String^ sId, String^ sPcFileName, String^ sDevFileName)
{
	int nRet = ERROR_INVALID_PARAMETER;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" push \"{0}\" \"{1}\"", sPcFileName, sDevFileName);
	if (nEchoType == 1)
	{
		array<String^>^ filelines = System::IO::File::ReadAllLines(sPcFileName);
		String^ sData = String::Join("\\n", filelines);
		logIt(sData);
		sparm = String::Format(" shell \"echo '{0}' > \"{1}\" \"", sData, sDevFileName);
	}
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	if ((nRet = runExe->startRunExe(gcnew String(adbpath), sparm, g_nTimeout)) == 0)
	{
		if (nEchoType == 1)
			return nRet;
		nRet = ERROR_NOT_FOUND;
		for each (String^ s in runExe->getExeStdOut()->ToArray())
		{
			logIt(s);	
			if (s->IndexOf("failed to copy") >= 0 || s->IndexOf("Permission denied") > 0)
			{
				nRet = ERROR_ACCESS_DENIED;
				break;
			}
			else {
				//346 KB/s (2484 bytes in 0.007s)
				Regex^ rx = gcnew Regex("\\d+\\s.*?[/]s\\s\\(\\d+\\s.*?s\\)",
					RegexOptions::Compiled | RegexOptions::IgnoreCase);
				if (rx->IsMatch(s))
				{
					nRet = ERROR_SUCCESS;
					break;
				}
			}
		}
	}
	else
	{
		for each (String^ s in runExe->getExeStdOut())
		{
			logIt(s);
		}
	}
	return nRet;
}

int UploadFile(String^ sType, String^ sId, String^ sPcFileName, String^ sDevFileName, String^ sBundle)
{
	logIt(String::Format("UploadFile++ sType={0},sID={1},pcfile={2}, devfile={3}", sType, sId, sPcFileName, sDevFileName));
	int nRet = ERROR_NOT_FOUND;
	if (String::Compare(sType, "Apple", TRUE) == 0)
	{
		nRet = UploadAppleFile(sId, sPcFileName, sDevFileName, sBundle);
	}
	else if (String::Compare(sType, "Android", TRUE) == 0)
	{
		nRet = UploadAndroidFile(sId, sPcFileName, sDevFileName);
	}
	return nRet;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//download file
//

int DownloadAndroidFile(String^ sId, String^ sPcFileName, String^ sDevFileName)
{
	int nRet = ERROR_INVALID_PARAMETER;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" pull \"{1}\" \"{0}\"", sPcFileName, sDevFileName);
	if (nEchoType == 1)
	{
		sparm = String::Format(" shell cp {1} /data/local/tmp ", sPcFileName);
		if (!String::IsNullOrEmpty(sId))
		{
			sparm = String::Format("-s {0} ", sId) + sparm;
		}
		if ((nRet = runExe->startRunExe(gcnew String(adbpath), sparm, g_nTimeout)) == 0)
		{
			sparm = String::Format(" pull \"/data/local/tmp/{1}\" \"{0}\"", sPcFileName, System::IO::Path::GetFileName(sDevFileName));
		}
		else
			return nRet;
	}
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	if ((nRet = runExe->startRunExe(gcnew String(adbpath), sparm, g_nTimeout)) == 0)
	{
		if (nEchoType == 1)
			return nRet;
		nRet = ERROR_NOT_FOUND;
		for each (String^ s in runExe->getExeStdOut()->ToArray())
		{
			logIt(s);
			if (s->IndexOf("Permission denied") > 0)
			{
				nRet = ERROR_ACCESS_DENIED;
				break;
			}
			else {
				//346 KB/s (2484 bytes in 0.007s)
				Regex^ rx = gcnew Regex("\\d+\\s.*?[/]s\\s\\(\\d+\\s.*?s\\)",
					RegexOptions::Compiled | RegexOptions::IgnoreCase);
				if (rx->IsMatch(s))
				{
					nRet = ERROR_SUCCESS;
					break;
				}
			}
		}
	}
	else
	{
		for each (String^ s in runExe->getExeStdOut())
		{
			logIt(s);
		}
	}
	return nRet;
}

int DownloadAppleFile(String^ sId, String^ sPcFileName, String^ sDevFileName, String^ sBundle)
{
	int nRet = ERROR_INVALID_PARAMETER;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format("-u {0} --download --local \"{1}\" --remote \"{2}\" ", sId, sPcFileName, sDevFileName);
	if (!String::IsNullOrEmpty(sBundle))
	{
		sparm = sparm + String::Format(" --domain {0}", sBundle);
	}
	
	nRet = runExe->startRunExe(gcnew String(idevceutilcorepath), sparm, g_nTimeout);

	return nRet;
}

int DownloadFile(String^ sType, String^ sId, String^ sPcFileName, String^ sDevFileName, String^ sBundle)
{
	logIt(String::Format("DownloadFile++ sType={0},sID={1},pcfile={2}, devfile={3}", sType, sId, sPcFileName, sDevFileName));
	int nRet = ERROR_NOT_FOUND;
	if (String::Compare(sType, "Apple", TRUE) == 0)
	{
		nRet = DownloadAppleFile(sId, sPcFileName, sDevFileName, sBundle);
	}
	else if (String::Compare(sType, "Android", TRUE) == 0)
	{
		nRet = DownloadAndroidFile(sId, sPcFileName, sDevFileName);
	}
	return nRet;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//check Status

int CheckStatus(String^ sType, int nPort, String^ Hubname, String^ sId, String^ sDevFileName, String^ sBundle, int nIntervalTime)
{
	int nRet = ERROR_SUCCESS;
	while (TRUE)
	{
		pin_ptr<const TCHAR> content = PtrToStringChars(Hubname);
		if (!Utiltools::IsConnectDev(nPort, (TCHAR *)content))
		{
			logIt("no device connected.");
			nRet = ERROR_DEVICE_NOT_CONNECTED;
			break;
		}
		if (String::Compare(sType, "Apple", TRUE) == 0)
		{
			Sleep(nIntervalTime);
			int err = ERROR_SUCCESS;
			if ((err = PhoneAppleFileExist(sId, sDevFileName, sBundle))==ERROR_SUCCESS)
			{
				logIt("find device files.");
				nRet = ERROR_FILE_EXISTS;
				break;
			}
			if (err == 0xE800005B)
			{
				logIt("kAMDServiceLimitError, need restart phone.");
				nRet = 0xE800005B;
				break;
			}
		}
		else if (String::Compare(sType, "Android", TRUE) == 0)
		{
			if (PhoneAndroidFileExist(sId, sDevFileName)==ERROR_SUCCESS)
			{
				logIt("find device files.");
				nRet = ERROR_FILE_EXISTS;
				break;
			}
		}
		
		Sleep(5000);
	}
	return nRet;
}

/////////////////////////////////////////////////////////////////////////////////////////
//File Delete
//
int PhoneAppleFileDelete(String^ sId, String^ sPhoneFileName, String^ sBundle)
{
	int bRet = ERROR_SUCCESS;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" -u {1} --delete {0} ", sPhoneFileName, sId);
	if (!String::IsNullOrEmpty(sBundle))
		sparm += String::Format(" --domain {0}", sBundle);

	if ((bRet = runExe->startRunExe(gcnew String(idevceutilcorepath), sparm, g_nTimeout)) == ERROR_SUCCESS)
	{
	}

	return bRet;
}

int PhoneAndroidFileDelete(String^ sId, String^ sPhoneFileName)
{
	int bRet = ERROR_NOT_FOUND;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" shell rm -f {0}", sPhoneFileName);
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	if ((bRet = runExe->startRunExe(gcnew String(adbpath), sparm, g_nTimeout)) == ERROR_SUCCESS)
	{
		
	}
	else
	{
		for each (String^ s in runExe->getExeStdOut()->ToArray())
		{
			logIt(s);
		}
	}

	return bRet;
}

int PhoneFileDelete(String^ sType, String^ sId, String^ sPhoneFileName, String^ sBundle)
{
	int bRet = ERROR_NOT_FOUND;
	logIt(String::Format("PhoneFileDelete++ sType={0},sID={1},phonefile={2}", sType, sId, sPhoneFileName));
	if (String::Compare(sType, "Apple", TRUE) == 0)
	{
		bRet = PhoneAppleFileDelete(sId, sPhoneFileName, sBundle);
	}
	else if (String::Compare(sType, "Android", TRUE) == 0)
	{
		bRet = PhoneAndroidFileDelete(sId, sPhoneFileName);
	}
	return bRet;
}
